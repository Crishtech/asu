<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomjaw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komjaw', function (Blueprint $table) {
            $table->increments('id');
            $table->string('isi',255);
            $table->date('tanggal_dibuat');
            $table->integer('jawaban_id');
            $table->integer('profil_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komjaw');
    }
}
